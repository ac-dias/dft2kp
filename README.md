# DFT2kp

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![PyPI](https://img.shields.io/pypi/v/dft2kp)](https://pypi.org/project/dft2kp/)

A numerical framework to explicitly calculate kp matrix elements from *ab-initio* data.

**Documentation** available at [dft2kp.gitlab.io/dft2kp](https://dft2kp.gitlab.io/dft2kp/)

**Code repository** available at [gitlab.com/dft2kp/dft2kp](https://gitlab.com/dft2kp/dft2kp)

## Current features

- Calculates kp matrix elements using the DFT eigenstates from [Quantum Espresso (QE)](https://gitlab.com/QEF/q-e).
- Folding down of the effective Hamiltonian into a set of selected bands.
- Rotates the numeriacal basis into a representation informed by the user.

## See also

- List of authors: [AUTHORS.md](https://gitlab.com/dft2kp/dft2kp/-/blob/main/AUTHORS.md)
- References to cite if we use our code: [CITING.md](https://gitlab.com/dft2kp/dft2kp/-/blob/main/CITING.md)
- Quick install intructions and compatibility requirements: [INSTALL.md](https://gitlab.com/dft2kp/dft2kp/-/blob/main/INSTALL.md)




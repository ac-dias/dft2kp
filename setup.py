from setuptools import setup, find_packages

python_version = '>=3.7'

install_requires = [
    'numpy',
    'scipy',
    'sympy<=1.10',
    'tabulate',
    'qsymm',
    'matplotlib',
    'irrep',
    'irreptables'
]

# uses README.md as the package long description
with open("README.md") as f:
    long_description = f.read()

# read version from module
exec(open('pydft2kp/__version.py').read())

setup(
    name="dft2kp",
    description="Calculates kp model and coefficients from DFT ab initio data.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/dft2kp/dft2kp",
    author="Gerson J. Ferreira",
    author_email="gersonjferreira@ufu.br",
    version=__version__,
    packages=find_packages('.'),
    install_requires=install_requires,
    python_requires=python_version)

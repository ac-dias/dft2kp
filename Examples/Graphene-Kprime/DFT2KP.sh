#!/bin/bash

source /opt/intel/oneapi/setvars.sh

echo 'SCF'
/home/gerson/QE2KP/q-e-with-kp/build/bin/pw.x < scf.in > scf.out

echo 'NSCF'
/home/gerson/QE2KP/q-e-with-kp/build/bin/pw.x < nscf.in > nscf.out

echo 'BANDS'
/home/gerson/QE2KP/q-e-with-kp/build/bin/bands.x < bands.in > bands.out

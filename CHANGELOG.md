# Changelog

## version 1.0.0

- Code published in SciPost Physics Codebases 25 (2024)
- New Examples CsPbCl3 (perovskite), InAs (zincblende), HgI2 (monolayer, bilayer), Bi2Se2Te (TI)
- New flag fixKpt on the irrep wrapper: searches for refUC/shiftUC that transforms the DFT k into the one from the equivalent one in the irrep tables.
- Set diagonal=False by default. It should be used only when qsymm symmetries are built from a set of irreps, and it cannot be used when the reps are reducible
- Updated to irrep 1.8.2: leaving the requirements version free
- List of anti-unitary symmetries is cleared by default
- Fix on symmetry identifications: check both signs of rotation axis and angle
- Fix on symmetry identifications: convertpi now returns float instead of sympy object
- Bi2Se3 example now compares convergence with DOS per irrep (testing purposes)
- reading efermi with the irrep package (our xml reader now reads only alat)
- Other minor fixes

## Version 0.0.3

- Fix requirements conflicts between qsymm and sympy, by requiring sympy=1.10
- Fix broken pypi wheel

## Version 0.0.2

- Fix badges
- Try to fix broken pypi wheel from version 0.0.2 (unsuccessful)

## Version 0.0.1

- First version submitted to pypi and for publication

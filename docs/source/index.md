```{include} ../../README.md
:end-line: 17
```

```{include} ../../AUTHORS.md
:heading-offset: 1
``` 

```{include} ../../CITING.md
:heading-offset: 1
``` 

%- {ref}`genindex`
%- {ref}`modindex`
%- {ref}`search`

```{toctree}
:maxdepth: 1
:hidden:

installation
quickstart
examplesstatus
reference
changelog
```
